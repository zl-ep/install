set -e

sh_DIR=$(cd $(dirname $0) && pwd )
echo $sh_DIR

git config --global user.name "zl-ep"
git config --global user.email "zhuyonfeng@ep-ep.com"
ssh-keygen -t rsa -C "zhuyonfeng@ep-ep.com"
echo "https://zl-ep:zl-ep1234@gitlab.com" > ~/.git-credentials
git config --global credential.helper store

# 1.install deb for lidar,imu,localization,mapping,monitor,slamService,tf,xmovermsgs.
git clone https://gitlab.com/zhuyongfeng/install-deb.git
#cd install-deb/
#git checkout 220414
sh $sh_DIR/install-deb/install_deb.sh
sudo rm -rf $sh_DIR/install-deb

# 2.install peception、saveBag、cargoPos、cargoDetect、guard
git clone https://gitlab.com/yangsc/ep.git
cp -frap ep/ /var/xmover
sudo rm -r ep

# 3.install  autocalib
git clone http://10.0.0.99:8090/songchao.yang/autocalib.git
mv autocalib/ ~/



sudo rm ~/.git-credentials

cd $sh_DIR
cd ..
sudo rm -rf install/

echo "All done."

